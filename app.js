// Dependencies
let express = require('express');
let path = require('path');
let logger = require('morgan');
let bodyParser = require('body-parser');
let AuthHelper = require('./api/auth/authHelper');
let cors = require('cors');
let CONFIG = require('config');

// Mongo
let mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
mongoose.connect(CONFIG.dbUri, {promiseLibrary: require('bluebird')})
	.then(() => console.log('connection succesful'))
	.catch((err) => console.error(err));


// App
let app = express();
app.use(cors());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({'extended': 'false'}));
app.use(express.static(path.join(__dirname, 'client/dist')));
// UI ROUTES
app.use('/', express.static(path.join(__dirname, 'client/dist')));

// API ROUTES
let AccountRouter = require('./api/routes/account');
let CategoryRouter = require('./api/routes/category');
let ReferenceRouter = require('./api/routes/reference');
let TransactionRouter = require('./api/routes/transaction');
let BudgetRouter = require('./api/routes/budget');
let AuthenticationRouter = require('./api/routes/auth');

app.use('/api/accounts', AccountRouter);
app.use('/api/categories', CategoryRouter);
app.use('/api/references', ReferenceRouter);
app.use('/api/transactions', TransactionRouter);
app.use('/api/budgets', BudgetRouter);
app.use('/api', AuthenticationRouter);

AuthHelper.initializeJWT();
app.set('view engine', 'html');

// catch 404 and forward to error handler
app.use(function (req, res, next) {
	let err = new Error('Not Found');
	res.status(404).send({});
});

// error handler
app.use(function (err, req, res, next) {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = req.app.get('env') == 'development' ? err : {};

	// render the error page
	res.status(err.status || 500);
	res.json(err);
});

module.exports = app;
