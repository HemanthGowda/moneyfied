import Vue from 'vue'
import RegisterView from '@/views/auth/RegisterView'

describe('RegisterView.vue', () => {
	let vm;
	beforeEach(() => {
		const Constructor = Vue.extend(RegisterView);
		vm = new Constructor({}).$mount();
	});

	it('should render image', () => {
		expect(vm.$el.querySelector('img').getAttribute('src')).to.exist;
	});

	it('should render register button', function () {
		expect(vm.$el.querySelector('button.right')).to.exist;
	});
});
