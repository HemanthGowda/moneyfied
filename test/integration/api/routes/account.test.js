let Account = require('../../../../api/models/Account');
let User = require('../../../../api/models/User');
let mongoose = require('mongoose');
var Chance = require('chance'),
	chance = new Chance();

describe('Accounts', () => {
	let token, user;
	beforeEach((done) => {
		var agent = chai.request.agent(server);
		agent.post('/api/register')
			.send({name: 'test', email: 'test@local.local', password: '123'})
			.then((res) => {
				user = res.body;
				agent.post('/api/login')
					.send({email: 'test@local.local', password: '123'})
					.then((res) => {
						token = res.body.token;
						done()
					});
			})
	});

	afterEach((done) => {
		mongoose.connection.db.dropDatabase(() => {
			done();
		});
	});

	describe('POST accounts', () => {
		let accountData;
		beforeEach(() => {
			accountData = {name: chance.string(), currency: chance.string(), user: user._id};
		});

		it('should create new account', (done) => {
			chai.request(server)
				.post('/api/accounts')
				.set('Authorization', token)
				.send(accountData)
				.end((err, res) => {
					res.should.have.status(201);
					res.body.should.be.a('object');
					res.body._id.should.to.not.be.null;
					res.body.name.should.to.equal(accountData.name);
					res.body.currency.should.to.equal(accountData.currency);
					res.body.user.should.to.equal(user._id);
					res.body.balance.should.to.equal(0);
					done();
				});
		});

		it('should create new account with balance', (done) => {
			accountData.balance = chance.floating();
			chai.request(server)
				.post('/api/accounts')
				.set('Authorization', token)
				.send(accountData)
				.end((err, res) => {
					res.should.have.status(201);
					res.body.should.be.a('object');
					res.body._id.should.to.not.be.null;
					res.body.name.should.to.equal(accountData.name);
					res.body.currency.should.to.equal(accountData.currency);
					res.body.user.should.to.equal(user._id);
					res.body.balance.should.to.equal(accountData.balance);
					done();
				});
		});

		describe('validations', () => {
			it('should fail when account name is not provided', (done) => {
				delete accountData.name;
				chai.request(server)
					.post('/api/accounts')
					.set('Authorization', token)
					.send(accountData)
					.end((err, res) => {
						res.should.have.status(201);
						res.body.should.be.a('object');
						res.body._id.should.to.not.be.null;
						res.body.name.should.to.equal(accountData.name);
						res.body.currency.should.to.equal(accountData.currency);
						res.body.user.should.to.equal(user._id);
						res.body.balance.should.to.equal(accountData.balance);
						done();
					});
			});
		})
	});
});
