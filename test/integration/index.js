//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

//Require the dev-dependencies
chai = require('chai');
chaiHttp = require('chai-http');
server = require('../../bin/www');
should = chai.should();
expect = chai.expect;

chai.use(chaiHttp);

require('./api/routes/account.test.js');
