let express = require('express');
let Budget = require('../models/Budget');
let passport = require('passport');
let permit = require("../auth/permission");
let router = express.Router();

/* GET ALL BUDGET */
router.get('/', [passport.authenticate('jwt', {session: false}), permit(["admin", "user"])], (req, res, next) => {
	Budget.find({user: req.user.id}, (err, products) => {
		if (err) return next(err);
		res.json(products);
	});
});

/* GET A BUDGET BY ID */
router.get('/:id', [passport.authenticate('jwt', {session: false}), permit(["admin", "user"])], (req, res, next) => {
	Budget.findOne({_id: req.params.id, user: req.user.id}, (err, post) => {
		if (err) return next(err);
		res.json(post);
	});
});

/* CREATE A NEW BUDGET */
router.post('/', [passport.authenticate('jwt', {session: false}), permit(["admin"])], (req, res, next) => {
	let payload = req.body;
	payload.user = req.user._id;
	Budget.create(payload, (err, post) => {
		if (err) return next(err);
		res.status(201).json(post);
	});
});

/* UPDATE A BUDGET */
router.put('/:id', [passport.authenticate('jwt', {session: false}), permit(["admin"])], (req, res, next) => {
	let conditions = {_id: req.params.id, user: req.user.id};
	let options = {new: true, runValidators: true};
	Budget.findOneAndUpdate(conditions, req.body, options, (err, post) => {
		if (err) return next(err);
		res.json(post);
	});
});

/* DELETE A BUDGET */
router.delete('/:id', [passport.authenticate('jwt', {session: false}), permit(["admin"])], (req, res, next) => {
	let conditions = {_id: req.params.id, user: req.user.id};
	let payload = {deletedAt: new Date(), deletedBy: req.user};
	let options = {new: true, runValidators: true};
	Budget.findOneAndUpdate(conditions, payload, options, (err, post) => {
		if (err) return next(err);
		res.json(post);
	});
});

module.exports = router;
