let express = require('express');
let Account = require('../models/Account');
let passport = require('passport');
let permit = require("../auth/permission");

let router = express.Router();

/* GET ALL ACCOUNTS */
router.get('/', [passport.authenticate('jwt', {session: false}), permit(["admin", "user"])], (req, res, next) => {
	Account.find({user: req.user.id}, (err, products) => {
		if (err) return next(err);
		res.json(products);
	});
});

/* GET AN ACCOUNT BY ID */
router.get('/:id', [passport.authenticate('jwt', {session: false}), permit(["admin", "user"])], (req, res, next) => {
	Account.findOne({_id: req.params.id, user: req.user.id}, (err, post) => {
		if (err) return next(err);
		res.json(post);
	});
});

/* CREATE A NEW ACCOUNT */
router.post('/', [passport.authenticate('jwt', {session: false}), permit(["admin"])], (req, res, next) => {
	let payload = req.body;
	payload.user = req.user._id;
	Account.create(payload, function (err, post) {
		if (err) return next(err);
		res.status(201).json(post);
	});
});

/* UPDATE AN ACCOUNT */
router.put('/:id', [passport.authenticate('jwt', {session: false}), permit(["admin"])], (req, res, next) => {
	let conditions = {_id: req.params.id, user: req.user.id};
	let options = {new: true, runValidators: true};
	Account.findOneAndUpdate(conditions, req.body, options, (err, post) => {
		if (err) return next(err);
		res.json(post);
	});
});

/* DELETE AN ACCOUNT */
router.delete('/:id', [passport.authenticate('jwt', {session: false}), permit(["admin"])], (req, res, next) => {
	let conditions = {_id: req.params.id, user: req.user.id};
	let payload = {deletedAt: new Date(), deletedBy: req.user};
	let options = {new: true, runValidators: true};
	Account.findOneAndUpdate(conditions, payload, options, (err, post) => {
		if (err) return next(err);
		res.json(post);
	});
});

module.exports = router;
