let jwt = require('jsonwebtoken');
let express = require('express');
let User = require("../models/User");

let router = express.Router();

// REGISTER AN USER
router.post('/register', (req, res, next) => {
	if (!req.body.email || !req.body.password) {
		res.status(400).send({error: 'Please pass email and password.'});
	} else {
		let newUser = new User(req.body);
		// save the user
		newUser.save((err) => {
			if (err) {
				return res.status(400).send({error: 'Email already exists.'});
			}
			res.json(newUser);
		});
	}
});

// LOGIN
router.post('/login', (req, res) => {
	User.findOne({
		email: req.body.email
	}, (err, user) => {
		if (err) throw err;

		if (!user) {
			res.status(401).send({error: 'User not found.'});
		} else {
			// check if password matches
			user.comparePassword(req.body.password, (err, isMatch) => {
				if (isMatch && !err) {
					// if user is found and password is right create a token
					let token = jwt.sign(user.toObject(), process.env.JWT_SECRET);
					// return the information including token as JSON
					res.json({token: 'JWT ' + token});
				} else {
					res.status(401).send({error: 'Wrong password.'});
				}
			});
		}
	});
});

// LOGOUT
router.get('/logout', (req, res) => {
	req.logout();
	res.json({});
});

module.exports = router;
