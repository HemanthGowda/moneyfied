let express = require('express');
let Category = require('../models/Category');
let passport = require('passport');
let permit = require("../auth/permission");
let router = express.Router();

/* GET ALL CATEGORIES */
router.get('/', [passport.authenticate('jwt', {session: false}), permit(["admin", "user"])], (req, res, next) => {
	Category.find({user: req.user.id}, (err, products) => {
		if (err) return next(err);
		res.json(products);
	});
});

/* GET A CATEGORY BY ID */
router.get('/:id', [passport.authenticate('jwt', {session: false}), permit(["admin", "user"])], (req, res, next) => {
	Category.findOne({_id: req.params.id, user: req.user.id}, (err, post) => {
		if (err) return next(err);
		res.json(post);
	});
});

/* CREATE A NEW CATEGORY */
router.post('/', [passport.authenticate('jwt', {session: false}), permit(["admin"])], (req, res, next) => {
	let payload = req.body;
	payload.user = req.user._id;
	Category.create(payload, (err, post) => {
		if (err) return next(err);
		res.status(201).json(post);
	});
});

/* UPDATE A CATEGORY */
router.put('/:id', [passport.authenticate('jwt', {session: false}), permit(["admin"])], (req, res, next) => {
	let conditions = {_id: req.params.id, user: req.user.id};
	let options = {new: true, runValidators: true};
	Category.findOneAndUpdate(conditions, req.body, options, (err, post) => {
		if (err) return next(err);
		res.json(post);
	});
});

/* DELETE A CATEGORY */
router.delete('/:id', [passport.authenticate('jwt', {session: false}), permit(["admin"])], (req, res, next) => {
	let conditions = {_id: req.params.id, user: req.user.id};
	let payload = {deletedAt: new Date(), deletedBy: req.user};
	let options = {new: true, runValidators: true};
	Category.findOneAndUpdate(conditions, payload, options, (err, post) => {
		if (err) return next(err);
		res.json(post);
	});
});

module.exports = router;
