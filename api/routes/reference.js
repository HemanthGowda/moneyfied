let express = require('express');
let Reference = require('../models/Reference');
let passport = require('passport');
let permit = require("../auth/permission");
let router = express.Router();

/* GET ALL REFERENCES */
router.get('/', [passport.authenticate('jwt', {session: false}), permit(["admin", "user"])], (req, res, next) => {
	Reference.find({user: req.user.id}, (err, references) => {
		if (err) return next(err);
		res.json(references);
	});
});

/* GET A REFERENCE BY ID */
router.get('/:id', [passport.authenticate('jwt', {session: false}), permit(["admin", "user"])], (req, res, next) => {
	Reference.findOne({_id: req.params.id, user: req.user.id}, (err, reference) => {
		if (err) return next(err);
		res.json(reference);
	});
});

/* CREATE A NEW REFERENCE */
router.post('/', [passport.authenticate('jwt', {session: false}), permit(["admin"])], (req, res, next) => {
	let payload = req.body;
	payload.user = req.user._id;
	payload.createdBy = req.user._id;
	Reference.create(payload, (err, reference) => {
		if (err) return next(err);
		res.status(201).json(reference);
	});
});

/* UPDATE A REFERENCE */
router.put('/:id', [passport.authenticate('jwt', {session: false}), permit(["admin"])], (req, res, next) => {
	let conditions = {_id: req.params.id, user: req.user.id};
	let options = {new: true, runValidators: true};
	Reference.findOneAndUpdate(conditions, req.body, options, (err, updatedReference) => {
		if (err) return next(err);
		res.json(updatedReference);
	});
});

/* DELETE A REFERENCE */
router.delete('/:id', [passport.authenticate('jwt', {session: false}), permit(["admin"])], (req, res, next) => {
	let conditions = {_id: req.params.id, user: req.user.id};
	let payload = {deletedAt: new Date(), deletedBy: req.user};
	let options = {new: true, runValidators: true};
	Reference.findOneAndUpdate(conditions, payload, options, (err, reference) => {
		if (err) return next(err);
		res.json(reference);
	});
});

module.exports = router;
