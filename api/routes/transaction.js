let express = require('express');
let Transaction = require('../models/Transaction');
let passport = require('passport');
let DateRangeFilter = require('../filters/dateFilter');
const url = require('url');
let permit = require("../auth/permission");
let router = express.Router();

/* GET ALL TRANSACTIONS */
router.get('/', [passport.authenticate('jwt', {session: false}), permit(["admin", "user"])], (req, res, next) => {
	const parsedUrl = url.parse(req.url, true);
	let query = {user: req.user.id};
	query = DateRangeFilter.applyFilter(query, parsedUrl.query, 'transactionDate');
	Transaction.find(query, (err, products) => {
		if (err) return next(err);
		res.json(products);
	});
});

/* GET A TRANSACTION BY ID */
router.get('/:id', [passport.authenticate('jwt', {session: false}), permit(["admin", "user"])], (req, res, next) => {
	Transaction.findOne({_id: req.params.id, user: req.user.id}, (err, post) => {
		if (err) return next(err);
		res.json(post);
	});
});

/* CREATE A NEW TRANSACTION */
router.post('/', [passport.authenticate('jwt', {session: false}), permit(["admin"])], (req, res, next) => {
	let payload = req.body;
	payload.user = req.user._id;
	Transaction.create(payload, (err, post) => {
		if (err) return next(err);
		res.status(201).json(post);
	});
});

/* UPDATE A TRANSACTION */
router.put('/:id', [passport.authenticate('jwt', {session: false}), permit(["admin"])], (req, res, next) => {
	let conditions = {_id: req.params.id, user: req.user.id};
	let options = {new: true, runValidators: true};
	Transaction.findOneAndUpdate(conditions, req.body, options, (err, post) => {
		if (err) return next(err);
		res.json(post);
	});
});

/* DELETE A TRANSACTION */
router.delete('/:id', [passport.authenticate('jwt', {session: false}), permit(["admin"])], (req, res, next) => {
	let conditions = {_id: req.params.id, user: req.user.id};
	let payload = {deletedAt: new Date(), deletedBy: req.user};
	let options = {new: true, runValidators: true};
	Transaction.findOneAndUpdate(conditions, payload, options, (err, post) => {
		if (err) return next(err);
		res.json(post);
	});
});

module.exports = router;
