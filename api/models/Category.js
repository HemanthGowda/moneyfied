let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let diffHistory = require('mongoose-diff-history/diffHistory');

let CategorySchema = new Schema({
    name: {
        type: String,
        index: true,
        required: true
    },
    type: {
        type: String,
        enum: ['income', 'expense'],
        required: true,
        index: true
    },
    icon: {
        type: String
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        index: true,
        required: true
    },
    parent: {
        type: Schema.Types.ObjectId,
        ref: 'Category',
        default: null,
        index: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    deletedAt: {
        type: Date
    },
    deletedBy: {
        type: Schema.Types.ObjectId,
        ref: 'User',
    }
});

CategorySchema.plugin(diffHistory.plugin);

module.exports = mongoose.model('Category', CategorySchema);
