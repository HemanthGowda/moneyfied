let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let diffHistory = require('mongoose-diff-history/diffHistory');

let TransactionSchema = new Schema({
	type: {
		type: String,
		enum: ['income', 'expense'],
		required: true,
		index: true
	},
	amount: {
		type: Number,
		default: 0
	},
	user: {
		type: Schema.Types.ObjectId,
		ref: 'User',
		index: true,
		required: true
	},
	account: {
		type: Schema.Types.ObjectId,
		ref: 'Account',
		index: true,
		required: true
	},
	category: [
		{
			type: Schema.Types.ObjectId,
			ref: 'Category',
			required: true
		}
	],
	description: {
		type: String
	},
	reference: {
		type: Schema.Types.ObjectId,
		ref: 'Account',
		index: true
	},
	transactionDate: {
		type: Date,
		required: true,
		index: true
	},
	createdAt: {
		type: Date,
		default: Date.now
	},
	createdBy: {
		type: Date,
		default: Date.now
	},
	deletedAt: {
		type: Date
	},
	deletedBy: {
		type: Schema.Types.ObjectId,
		ref: 'User',
	}
});

TransactionSchema.plugin(diffHistory.plugin);

module.exports = mongoose.model('Transaction', TransactionSchema);
