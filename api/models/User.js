let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let bcrypt = require('bcrypt-nodejs');
let diffHistory = require('mongoose-diff-history/diffHistory');

let UserSchema = new Schema({
	name: {
		type: String,
		index: true,
		required: true
	},
	email: {
		type: String,
		required: true,
		index: true,
		unique: true,
		dropDups: true
	},
	role: {
		type: String
	},
	password: {
		type: String,
		required: true
	},
	createdAt: {
		type: Date,
		default: Date.now
	}
});

UserSchema.plugin(diffHistory.plugin);

UserSchema.pre('save', function (next) {
	let user = this;
	if (this.isModified('password') || this.isNew) {
		bcrypt.genSalt(10, (err, salt) => {
			if (err) {
				return next(err);
			}
			bcrypt.hash(user.password, salt, null, (err, hash) => {
				if (err) {
					return next(err);
				}
				user.password = hash;
				next();
			});
		});
	} else {
		return next();
	}
});

UserSchema.methods.comparePassword = function (password, next) {
	bcrypt.compare(password, this.password, (err, isMatch) => {
		if (err) {
			return next(err);
		}
		next(null, isMatch);
	});
};

module.exports = mongoose.model('User', UserSchema);
