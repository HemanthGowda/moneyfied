let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let diffHistory = require('mongoose-diff-history/diffHistory');

let ReferenceSchema = new Schema({
    name: {
        type: String,
        index: true,
        required: true
    },
    description: {
        type: String
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    createdBy: {
        type: Schema.Types.ObjectId,
        ref: 'User',
    },
    deletedAt: {
        type: Date
    },
    deletedBy: {
        type: Schema.Types.ObjectId,
        ref: 'User',
    }
});

ReferenceSchema.index({ name: 1, user: 1 }, { unique: true });
ReferenceSchema.plugin(diffHistory.plugin);

module.exports = mongoose.model('Reference', ReferenceSchema);
