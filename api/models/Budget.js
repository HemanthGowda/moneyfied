let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let diffHistory = require('mongoose-diff-history/diffHistory');

let BudgetSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true,
        dropDups: true
    },
    amount: {
        type: Number,
        default: 0
    },
    interval: {
        type: String,
        enum: ['weekly', 'monthly'],
        required: true
    },
    from: {
        type: Date,
        default: Date.now
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        index: true,
        required: true
    },
    categories: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Category'
        }
    ],
    createdAt: {
        type: Date,
        default: Date.now
    },
    deletedAt: {
        type: Date
    },
    deletedBy: {
        type: Schema.Types.ObjectId,
        ref: 'User',
    }
});

BudgetSchema.plugin(diffHistory.plugin);

module.exports = mongoose.model('Budget', BudgetSchema);
