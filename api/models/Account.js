let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let diffHistory = require('mongoose-diff-history/diffHistory');

let AccountSchema = new Schema({
    name: {
        type: String,
        index: true,
        required: true,
        unique: true,
        dropDups: true
    },
    description: {
        type: String
    },
    balance: {
        type: Number,
        default: 0
    },
    currency: {
        type: String,
        required: true
    },
    icon: {
        type: String
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        index: true,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    deletedAt: {
        type: Date
    },
    deletedBy: {
        type: Schema.Types.ObjectId,
        ref: 'User',
    }
});

AccountSchema.plugin(diffHistory.plugin);

module.exports = mongoose.model('Account', AccountSchema);
