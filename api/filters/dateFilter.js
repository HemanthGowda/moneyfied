let moment = require('moment');
let _ = require('underscore');

module.exports = {
	applyFilter(query, queryParams, databaseField) {
		if (_.isEmpty(queryParams[databaseField])) {
			return query;
		}

		let dateRange = queryParams[databaseField].split(',');
		let queryForField = query[databaseField] || {};
		if (!_.isEmpty(dateRange[0])) {
			queryForField['$gte'] = moment.unix(dateRange[0]).toDate();
		}
		if (!_.isEmpty(dateRange[1])) {
			queryForField['$lt'] = moment.unix(dateRange[1]).toDate();
		}

		if (!_.isEmpty(queryForField)) {
			query[databaseField] = queryForField;
		}
		return query;
	}
};
