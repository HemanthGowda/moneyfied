let JwtStrategy = require('passport-jwt').Strategy;
let ExtractJwt = require('passport-jwt').ExtractJwt;
let passport = require('passport');

// load up the user model
let User = require('../models/User');
let AuthHelper = {};

AuthHelper.initializeJWT = () => {
	let opts = {};
	opts.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme('JWT');
	opts.secretOrKey = process.env.JWT_SECRET;
	passport.use(new JwtStrategy(opts, (jwt_payload, done) => {
		User.findOne({id: jwt_payload.id}, (err, user) => {
			if (err) {
				return done(err, false);
			}
			if (user) {
				done(null, user);
			} else {
				done(null, false);
			}
		});
	}));
};

module.exports = AuthHelper;
