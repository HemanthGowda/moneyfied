let _ = require('underscore');

// middleware for doing role-based permissions
module.exports = function permit(allowedRoles) {
	// return a middleware
	return (req, res, next) => {
		if (req.user && _.contains(allowedRoles, req.user.role))
			next(); // role is allowed, so continue on the next middleware
		else {
			res.status(403).json({message: "Forbidden"}); // user is forbidden
		}
	}
};
