# Moneyfied (This project is in initial stage.)

> Moneyfied is a personal finance manager which keeps track of daily expenses and put them into clear and visualized categories such as Expense: Food, Shopping or Income: Salary, Gift.

## Demo
Find demo [here](https://gitlab.com/HemanthGowda/moneyfied/blob/master/static/Demo.gif)

## Build Setup

``` bash
# install dependencies
npm install

# set environment variables
JWT_SECRET: <any string (password)>
DB_HOST: <mongodb host>
DB_NAME: <database name>

# start application
npm start

# open application
http://localhost:3000/
```
