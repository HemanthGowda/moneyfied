'use strict'
const utils = require('./utils');
const webpack = require('webpack');
const merge = require('webpack-merge');
const path = require('path');
const baseWebpackConfig = require('./webpack.base.conf');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin');
const portfinder = require('portfinder');

const HOST = process.env.HOST;
const PORT = process.env.PORT && Number(process.env.PORT);

const devWebpackConfig = merge(baseWebpackConfig, {
	module: {
		rules: utils.styleLoaders({sourceMap: true, usePostCSS: true})
	},
	// cheap-module-eval-source-map is faster for development
	devtool: 'cheap-module-eval-source-map',

	// these devServer options should be customized in /config/index.js
	devServer: {
		clientLogLevel: 'warning',
		historyApiFallback: {
			rewrites: [
				{from: /.*/, to: path.posix.join('/', 'index.html')},
			],
		},
		hot: true,
		contentBase: false, // since we use CopyWebpackPlugin.
		compress: true,
		host: HOST || 'localhost',
		port: PORT || '8081',
		open: false,
		overlay: {warnings: false, errors: true},
		publicPath: '/',
		proxy: {},
		quiet: true, // necessary for FriendlyErrorsPlugin
		watchOptions: {
			aggregateTimeout: 500,
			poll: 1000,
		}
	},
	plugins: [
		new webpack.DefinePlugin({
			__CONFIG: JSON.stringify(require('config'))
		}),
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NamedModulesPlugin(), // HMR shows correct file names in console on update.
		new webpack.NoEmitOnErrorsPlugin(),
		// https://github.com/ampedandwired/html-webpack-plugin
		new HtmlWebpackPlugin({
			filename: 'index.html',
			template: 'index.html',
			inject: true
		}),
		// copy custom static assets
		new CopyWebpackPlugin([
			{
				from: path.resolve(__dirname, '../dist/static'),
				to: 'static',
				ignore: ['.*']
			}
		])
	]
});

module.exports = new Promise((resolve, reject) => {
	portfinder.basePort = process.env.PORT || 8081;
	portfinder.getPort((err, port) => {
		if (err) {
			reject(err)
		} else {
			// publish the new Port, necessary for e2e tests
			process.env.PORT = port;
			// add port to devServer config
			devWebpackConfig.devServer.port = port;

			// Add FriendlyErrorsPlugin
			devWebpackConfig.plugins.push(new FriendlyErrorsPlugin({
				compilationSuccessInfo: {
					messages: [`Your application is running here: http://${devWebpackConfig.devServer.host}:${port}`],
				},
				onErrors: utils.createNotifierCallback()
			}));

			resolve(devWebpackConfig)
		}
	})
});

module.exports = devWebpackConfig;
