export default {
	onLoginSuccess(response) {
		localStorage.setItem('TOKEN', response.data.token);
	},

	onLogout() {
		localStorage.removeItem('TOKEN');
	},

	isAuthenticated() {
		let token = localStorage.getItem('TOKEN');
		return !!token;
	},

	getToken() {
		return localStorage.getItem('TOKEN')
	}
}
