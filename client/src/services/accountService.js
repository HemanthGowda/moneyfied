import {Accounts} from "../models/account";
import axios from "axios";

export default {
	async fetchAll() {
		return await axios.get(__CONFIG.baseUrl + '/api/accounts')
			.then(response => {
				return new Accounts(response.data);
			}, this)
			.catch(e => {
				throw e;
			}, this);
	}
}
