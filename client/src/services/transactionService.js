import axios from "axios";
import {Transactions} from "../models/transaction";

export default {
	async fetchAll(filter) {
		return await axios.get(__CONFIG.baseUrl + '/api/transactions', {params: filter})
			.then(response => {
				return new Transactions(response.data);
			}, this)
			.catch(e => {
				throw e;
			}, this);
	}
}
