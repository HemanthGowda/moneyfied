import {Collection, Model} from './model';
import _ from "underscore";

export function Transaction(data, options) {
	Model.call(this, data, options);
	this.url = () => {
		if (this._id) {
			return __CONFIG.baseUrl + '/api/transactions/' + this._id;
		}
		return __CONFIG.baseUrl + '/api/transactions';
	};

	this.getAttributes = () => {
		return {
			_id: this._id,
			type: this.type,
			amount: this.amount,
			account: this.account,
			category: this.category,
			description: this.description,
			reference: this.reference,
			transactionDate: this.transactionDate
		}
	};

	this.isDeleted = () => {
		return !_.isEmpty(this.deletedAt);
	}
}

export function Transactions(collection) {
	this.model = Transaction;
	Collection.call(this, collection);
	this.url = __CONFIG.baseUrl + '/api/transactions';
}
