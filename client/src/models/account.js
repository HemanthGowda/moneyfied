import {Collection, Model} from './model';

export function Account(data, options) {
	Model.call(this, data, options);
	this.url = () => {
		if (this._id) {
			return __CONFIG.baseUrl + '/api/accounts/' + this._id;
		}
		return __CONFIG.baseUrl + '/api/accounts';
	};

	this.getAttributes = () => {
		return {
			_id: this._id,
			name: this.name,
			description: this.description,
			currency: this.currency,
			icon: this.icon,
			balance: this.balance,
			user: this.user
		}
	};
}

export function Accounts(collection) {
	this.model = Account;
	Collection.call(this, collection);

	this.url = __CONFIG.baseUrl + '/api/accounts';
}
