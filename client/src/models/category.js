import {Collection, Model} from './model';
import _ from 'underscore';
import Constants from './constants'

export function Category(data, options) {
	Model.call(this, data, options);
	this.url = () => {
		if (this._id) {
			return __CONFIG.baseUrl + '/api/categories/' + this._id;
		}
		return __CONFIG.baseUrl + '/api/categories';
	};

	this.getAttributes = () => {
		return {
			_id: this._id,
			name: this.name,
			type: this.type,
			icon: this.icon,
			user: this.user,
			parent: this.parent
		}
	};
	this.isRootCategory = () => {
		return _.isEmpty(this.parent);
	};

	this.isExpenseCategory = () => {
		return this.type === Constants.CATEGORY.TYPE_EXPENSE;
	};

	this.isIncomeCategory = () => {
		return this.type === Constants.CATEGORY.TYPE_INCOME;
	};
}

export function Categories() {
	Collection.call(this);
	this.model = Category;
	this.url = __CONFIG.baseUrl + '/api/categories';

	this.getCategoriesTree = (type) => {
		let rootCategories = _.filter(this.models, (category) => {
			return category.isRootCategory() && category.type === type && !category.isDeleted();
		});

		return this._deriveSubCategories(rootCategories);
	};

	this._deriveSubCategories = (rootCategories) => {
		let rootCategoriesClone = [];
		_.each(rootCategories, (rootCategory) => {
			let rootCategoryClone = new Category(rootCategory.getAttributes());
			let subCategories = _.filter(this.models, (category) => {
				return category.parent === rootCategoryClone._id && !category.isDeleted()
			});

			if (subCategories && subCategories.length > 0) {
				rootCategoryClone.subCategories = this._deriveSubCategories(subCategories);
			}

			rootCategoriesClone.push(rootCategoryClone);
		});
		return rootCategoriesClone;
	};
}
