import {Collection, Model} from './model';

export function Reference(data, options) {
	Model.call(this, data, options);
	this.url = () => {
		if (this._id) {
			return __CONFIG.baseUrl + '/api/references/' + this._id;
		}
		return __CONFIG.baseUrl + '/api/references';
	};

	this.getAttributes = () => {
		return {
			_id: this._id,
			name: this.name,
			description: this.description,
			user: this.user
		}
	};
}

export function References() {
	Collection.call(this);
	this.model = Reference;
	this.url = __CONFIG.baseUrl + '/api/references';
}
