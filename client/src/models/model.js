import axios from 'axios'
import _ from 'underscore'
import $ from 'jquery';

export function Model(data, options) {
	this.parse = (modelData) => {
		_.extend(this, modelData);
	};

	this.parse(data);
	this.options = _.extend({}, options);

	this.save = () => {
		let deferred = $.Deferred();
		axios.post(this.url(), this.getAttributes())
			.then(response => {
				deferred.resolve(response);
			}, this)
			.catch(e => {
				deferred.reject(e)
			});

		return deferred.promise();
	};

	this.update = () => {
		let deferred = $.Deferred();
		axios.put(this.url(), this.getAttributes())
			.then(response => {
				deferred.resolve(response);
			}, this)
			.catch(e => {
				deferred.reject(e)
			});

		return deferred.promise();
	};

	this.destroy = () => {
		let deferred = $.Deferred();
		axios.delete(this.url())
			.then(response => {
				deferred.resolve(response);
			}, this)
			.catch(e => {
				deferred.reject(e);
			});

		return deferred.promise();
	};

	this.isDeleted = () => {
		return !_.isEmpty(this.deletedAt);
	};

}

export function Collection(collection) {
	this.models = [];
	if (collection && collection.length > 0) {
		_.each(collection, (modelData) => {
			this.models.push(new this.model(modelData));
		}, this);
	}

	this.fetch = () => {
		axios.get(this.url)
			.then(response => {
				this.models = [];
				_.each(response.data, (modelData) => {
					this.models.push(new this.model(modelData));
				}, this);
			}, this)
			.catch(e => {
				this.errors.push(e)
			}, this);

		return this;
	};

	this.add = (modelData) => {
		let existing = _.findWhere(this.models, {_id: modelData._id});
		if (existing) {
			existing.parse(modelData);
		} else {
			let newModel = new this.model(modelData);
			this.models.push(newModel);
		}
	};

	this.remove = (model) => {
		this.models = _.without(this.models, model);
	};

	this.where = (attributes) => {
		return _.where(this.models, attributes)
	}
}
