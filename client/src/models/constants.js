export default {
	CATEGORY: {
		TYPE_INCOME: 'income',
		TYPE_EXPENSE: 'expense'
	}
}