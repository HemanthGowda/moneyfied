import {Accounts} from './models/account';
import {Categories} from "./models/category";
import {References} from "./models/reference";
import {Transactions} from "./models/transaction";
import $ from 'jquery';
import _ from 'underscore';


export function Application() {
	this.isInitialized = false;
	this._collections = {};

	this.addCollection = (ns, collection) => {
		this._collections[ns] = collection;
	};

	this.fetchAccounts = () => {
		let accounts = new Accounts();
		return accounts.fetch();
	};

	this.fetchCategories = () => {
		let categories = new Categories();
		return categories.fetch();
	};

	this.fetchReferences = () => {
		let references = new References();
		return references.fetch();
	};

	this.fetchTransactions = () => {
		let references = new Transactions();
		return references.fetch();
	};

	this.setUpCollections = (accounts, categories, references, transactions) => {
		this.addCollection('accounts', accounts);
		this.addCollection('categories', categories);
		this.addCollection('references', references);
		this.addCollection('transactions', transactions);
	};

	this.initialize = () => {
		let initializePromise = $.Deferred();
		let fetchCalls = [
			this.fetchAccounts(),
			this.fetchCategories(),
			this.fetchReferences(),
			this.fetchTransactions()
		];
		let self = this;

		$.when.apply(this, fetchCalls).done((accounts, categories, references, transactions) => {
			self.setUpCollections(accounts, categories, references, transactions);
			self.isInitialized = true;
			initializePromise.resolve();
		});

		return initializePromise;
	};

	this.findCollection = (ns) => {
		return this._collections[ns];
	};

	this.findModel = (id, ns) => {
		let collection = this._collections[ns];
		return _.findWhere(collection.models, {_id: id});
	}
}
