import Vue from 'vue'
import Router from 'vue-router'

import Full from '@/views/Full'
import Login from '@/views/auth/LoginView'
import Register from '@/views/auth/RegisterView'
import ListAccounts from '@/views/accounts/AccountsView'
import CategoriesView from '@/views/categories/CategoriesView'
import ReferencesView from '@/views/references/ReferencesView'
import TransactionsView from '@/views/transactions/TransactionsView'
import DashboardView from '@/views/dashboard/Dashboard'
import Auth from '../services/auth'
import axios from 'axios';
import Promise from 'es6-promise'

Vue.use(Router);

let router = new Router({
	routes: [
		{
			path: '/login',
			name: 'Login',
			meta: {bodyClass: 'login'},
			component: Login
		},
		{
			path: '/register',
			name: 'Register',
			meta: {bodyClass: 'register'},
			component: Register
		},
		{
			path: '/',
			component: Full,
			redirect: '/dashboard',
			meta: {
				requiresAuth: true
			},
			children: [
				{
					path: 'dashboard',
					name: 'Dashboard',
					component: DashboardView
				},
			]
		},
		{
			path: '/accounts',
			component: Full,
			redirect: '/accounts/',
			meta: {
				requiresAuth: true
			},
			children: [
				{
					path: '/',
					name: 'Accounts',
					component: ListAccounts
				}
			]
		},
		{
			path: '/transactions',
			component: Full,
			redirect: '/transactions/',
			meta: {
				requiresAuth: true
			},
			children: [
				{
					path: '/',
					name: 'Transactions',
					component: TransactionsView
				}
			]
		},
		{
			path: '/categories',
			component: Full,
			redirect: '/categories/',
			meta: {
				requiresAuth: true
			},
			children: [
				{
					path: '/',
					name: 'Categories',
					component: CategoriesView
				}
			]
		},
		{
			path: '/references',
			component: Full,
			redirect: '/references/',
			meta: {
				requiresAuth: true
			},
			children: [
				{
					path: '/',
					name: 'References',
					component: ReferencesView
				}
			]
		}
	]
});
export default router;

router.beforeResolve((to, from, next) => {
	if (to.matched.some(record => record.meta.requiresAuth)) {
		if (!Auth.isAuthenticated()) {
			next({
				path: '/login'
			})
		} else {
			if (!window.app.isInitialized) {
				let promise = window.app.initialize();
				promise.then(() => {
					next();
				});
			} else {
				next()
			}

		}
	} else {
		next()
	}
});

axios.interceptors.response.use((response) => { // intercept the global error
	return response
}, (error) => {
	if (error.response.status === 401) {
		Auth.onLogout();
		router.go('/login');
	}
	return Promise.reject(error)
});

axios.interceptors.request.use((config) => {

	let token = Auth.getToken();

	if (token) {
		config.headers['Authorization'] = token;
	}

	return config;
}, (err) => {

	return Promise.reject(err);
});
